# frozen_string_literal: true

RSpec.describe 'Icons on page' do
  TestContainer.browser.imgs.each do |image|
    next if image.src.empty? or TestContainer.tested_links.include?(image.src)
    TestContainer.tested_links << image.src
    it "Image #{image.src} is found" do
      expect(Checker.link_status(image.src)).to be_between 200, 299
    end
  end
end
