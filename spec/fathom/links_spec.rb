# frozen_string_literal: true

RSpec.describe 'Links on page' do
  TestContainer.browser.links.each do |link|
    next if link.href.empty? or TestContainer.tested_links.include?(link.href)
    TestContainer.tested_links << link.href
    it "Link #{link.href} is found" do
      expect(Checker.link_status(link.href)).to be_between 200, 299
    end
  end
end