require 'watir'
require 'webdrivers'
require_relative 'checker'

class TestContainer
  CHROME_LOG_PATH=Dir.pwd + "/Chrome.log"
  @tested_links=[]
  class << self
    attr_accessor :browser
    attr_accessor :tested_links
  end
end

# Start the Chrome browser with output log details to the required Log file
TestContainer.browser = Watir::Browser.new :chrome, args: ['--disable-popup-blocking', '--disable-gpu', '--no-sandbox', '--disable-dev-shm-usage', '--enable-logging', '--v', "--log-file=#{TestContainer::CHROME_LOG_PATH}"]

TestContainer.browser.goto 'https://fathom.z8.web.core.windows.net'
# TestContainer.browser.goto 'https://integrationqa.gitlab.io/fathom/online'
# TestContainer.browser.goto 'https://www.fathom.plus/'

at_exit do
  TestContainer.browser.quit
end
