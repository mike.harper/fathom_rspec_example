This test will carry out the following validation against the Fathom website using Ruby and can run in a Docker environment;
   - Verify there are no broken icons
   - Verify there are no broken links
   - Check the log and report any required errors


# Prerequisites


## Install
Have installed:

* Chrome
* ruby

`gem install ffi`
`gem install page-object`
`gem install rake`
`gem install rest-client`
`gem install rspec`
`gem install rspec_junit_formatter`
`gem install selenium-webdriver`
`gem install watir`
`gem install webdrivers`
`gem install webrat`

`gem install bundler` Install bundler
`bundle install` - at root level to install dependencies


# Overall RSpec tests

To run tests
`bundle exec rspec`
or
`rspec`

