FROM ruby:2.6.6

# Required ruby gems
RUN mkdir /test-fathom
WORKDIR /test-fathom
COPY Gemfile /test-fathom/Gemfile
RUN gem install bundler
RUN bundle install

# Virtual Frame buffer
RUN apt-get update -y \
  && apt-get -y install \
    xvfb \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Google Chrome - Get the latest stable version
ARG CHROME_VERSION="google-chrome-stable"
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update -qqy \
  && apt-get -qqy install \
    ${CHROME_VERSION:-google-chrome-stable} \
  && rm /etc/apt/sources.list.d/google-chrome.list \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*


# Add language settings to handle special characters
RUN export LANG=C.UTF-8
RUN export LANGUAGE=C.UTF-8
RUN export LC_ALL=C.UTF-8

RUN ln -sf pkcs11/p11-kit-trust.so /usr/lib64/libnssckbi.so